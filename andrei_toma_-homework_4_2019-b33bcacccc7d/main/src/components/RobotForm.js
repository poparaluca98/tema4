import React, { Component } from "react";
import RobotList from "../components/RobotList";

class RobotForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      type: "",
      mass: 0
    };

    this.handleChange = e => {
      this.setState({
        [e.target.name]: e.target.value
      });
    };
  }
  render() {
    return (
      <div>
        <input
          type="text"
          placeholder="name"
          onChange={this.handleChange}
          name="name"
          id="name"
        />
        <input
          type="text"
          placeholder="type"
          onChange={this.handleChange}
          name="type"
          id="type"
        />
        <input
          type="number"
          placeholder="100"
          onChange={this.handleChange}
          name="mass"
          id="mass"
        />
        <input
          type="button"
          value="add"
          onClick={() =>
            this.props.onAdd({
              name: this.state.name,
              type: this.state.type,
              mass: this.state.mass
            })
          }
        />
      </div>
    );
  }
}
export default RobotForm;
